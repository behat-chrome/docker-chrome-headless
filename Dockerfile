# This should always be overridden from build variables.
# Configured in CI/CD variables in Gitlab.
ARG PHP_VERSION=8.3
FROM php:${PHP_VERSION}-fpm

ENV DISPLAY=:99 \
    DBUS_SESSION_BUS_ADDRESS=/dev/null \
    PATH="$PATH:/code/vendor/bin"

RUN sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /usr/local/etc/php-fpm.conf \
 && sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" /usr/local/etc/php-fpm.d/www.conf \
 && sed -i -e "s/pm.max_children = 5/pm.max_children = 9/g" /usr/local/etc/php-fpm.d/www.conf \
 && sed -i -e "s/pm.start_servers = 2/pm.start_servers = 3/g" /usr/local/etc/php-fpm.d/www.conf \
 && sed -i -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 2/g" /usr/local/etc/php-fpm.d/www.conf \
 && sed -i -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 4/g" /usr/local/etc/php-fpm.d/www.conf \
 && sed -i -e "s/pm.max_requests = 500/pm.max_requests = 200/g" /usr/local/etc/php-fpm.d/www.conf

RUN apt-get update -qqy \
  && apt-get -qqy install wget ca-certificates apt-transport-https nginx supervisor \
    unzip git x11vnc xfonts-100dpi xfonts-75dpi xfonts-scalable xvfb libpng-dev libjpeg-dev gnupg \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

RUN wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  # && apt-get -qqy install google-chrome-stable google-chrome-unstable chromium google-chrome-beta \
  && wget https://dl.google.com/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_127.0.6533.119-1_amd64.deb \
  && apt-get -qqy install ./google-chrome-stable_127.0.6533.119-1_amd64.deb \
  && rm -v google-chrome-stable_127.0.6533.119-1_amd64.deb \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

RUN docker-php-ext-install gd

RUN useradd headless --shell /bin/bash --create-home \
  && usermod -a -G sudo headless \
  && echo 'ALL ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers \
  && echo 'headless:nopassword' | chpasswd

RUN mkdir /data

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
 && rm -rf /etc/nginx/sites-enabled/default \
 && mkdir -p /root/.ssh \
 && echo "Host *\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

RUN pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/xdebug.ini

VOLUME /code

WORKDIR /code

COPY files/supervisord.conf /etc/supervisord.conf

COPY files/entrypoint.sh /entrypoint.sh

COPY files/vhost.conf /etc/nginx/sites-enabled/vhost.conf

ENTRYPOINT ["/entrypoint.sh"]

CMD ["bash"]
